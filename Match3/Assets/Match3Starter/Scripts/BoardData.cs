﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

using CandyCrushFake;

public class BoardData
{
	public class SpecialTileMakingInfo
	{
		public int type = 0;
		public Vector2 pos = Vector2.zero;
		public List<Vector2> clearedPosList = null;

		public SpecialTileMakingInfo(int _type, Vector2 _pos, List<Vector2> _clearedPosList)
		{
			type = _type;
			pos = _pos;
			clearedPosList = _clearedPosList;
		}
	}

	int[,] tiles = null;
	GameControl gameControl;
	List<int> allTileTypes = null;
	int xSize = 0;
	int ySize = 0;
	Vector2[] adjacentDirections = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };

	public BoardData(GameControl _gameControl, List<int> _allTileTypes)
	{
		gameControl = _gameControl;
		allTileTypes = _allTileTypes;
	}

	public void CreateBoard(int _xSize, int _ySize)
	{
		xSize = _xSize;
		ySize = _ySize;

		tiles = new int[xSize, ySize];

		int[] previousLeft = new int[ySize];
		for(int i = 0; i < previousLeft.Length; ++i)
		{
			previousLeft[i] = Constants.EMPTY_TILE;
		}

		int previousBelow = Constants.EMPTY_TILE; ;

		List<int> _possibleTiles = new List<int>();
		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				_possibleTiles.Clear();
				_possibleTiles.AddRange(allTileTypes);

				_possibleTiles.Remove(previousLeft[y]);
				_possibleTiles.Remove(previousBelow);

				int _tile = _possibleTiles[UnityEngine.Random.Range(0, _possibleTiles.Count)];
				tiles[x, y] = _tile;
				previousLeft[y] = _tile;
				previousBelow = _tile;
			}
		}
    }

	public int[,] GetBoardTable()
	{
		return tiles;
	}

	public bool CanMatchBySwap(Vector2 _basePos, Dir _dir)
	{
		bool _canMatch = false;

		// Swap 시뮬레이션
		Vector2 _targetPos = GetTilePosAboutDir(_basePos, _dir);
		Swap(_basePos, _targetPos);

		//
		Vector2[] _indexes = new Vector2[] { _basePos, _targetPos };
		List<Vector2> _matchingTiles = new List<Vector2>();
		for(int i = 0; i < _indexes.Length; ++i)
		{
			_matchingTiles.Clear();
			_matchingTiles.AddRange(FindMatch_horizontal(_indexes[i]));
			_matchingTiles.AddRange(FindMatch_Vertical(_indexes[i]));

			if(0 < _matchingTiles.Count)
			{
				_canMatch = true;
				break;
			}
		}

		// 원래대로 복원
		Swap(_basePos, _targetPos);

		return _canMatch;
	}

	public bool CanDragToDir(Vector2 _basePos, Dir _dir)
	{
		bool _canDrag = true;

		if(Dir.left == _dir)
		{
			_canDrag = (0 != (int)_basePos.x);
		}
		else if(Dir.right == _dir)
		{
			_canDrag = (xSize - 1 != (int)_basePos.x);
		}
		else if(Dir.up == _dir)
		{
			_canDrag = (ySize - 1 != (int)_basePos.y);
		}
		else if(Dir.down == _dir)
		{
			_canDrag = (0 != (int)_basePos.y);
		}

		return _canDrag;
	}

	public Vector2 GetTilePosAboutDir(Vector2 _basePos, Dir _dir)
	{
		Vector2 _offset = Vector2.zero;

		if(Dir.left == _dir)
		{
			_offset = Vector2.left;
		}
		else if(Dir.right == _dir)
		{
			_offset = Vector2.right;
		}
		else if(Dir.up == _dir)
		{
			_offset = Vector2.up;
		}
		else if(Dir.down == _dir)
		{
			_offset = Vector2.down;
		}

		return _basePos + _offset;
	}

	public List<Vector2> FindHintTileIndexes()
	{
		List<Vector2> _hintTileIndexes = new List<Vector2>();

		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				_hintTileIndexes = FindMatchBySwap(new Vector2(x, y));
				if(0 < _hintTileIndexes.Count)
				{
					return _hintTileIndexes;
                }
            }
		}

		return _hintTileIndexes;
	}

	public void Swap(Vector2 _aPos, Vector2 _bPos)
	{
		int _tempTile = tiles[(int)_aPos.x, (int)_aPos.y];
		tiles[(int)_aPos.x, (int)_aPos.y] = tiles[(int)_bPos.x, (int)_bPos.y];
		tiles[(int)_bPos.x, (int)_bPos.y] = _tempTile;
	}

	public List<SpecialTileMakingInfo> MakingSpecialTiles(Vector2 _pos)
	{
		List<SpecialTileMakingInfo> _specialTileInfos = new List<SpecialTileMakingInfo>();

		List<Vector2> _matchingHorizontalPosList = FindMatch_horizontal(_pos);
		if(3 <= _matchingHorizontalPosList.Count) // 기준 위치를 제외한 갯수
		{
			_matchingHorizontalPosList.Add(_pos);

			int _maxLeftIndex = 0;
			for(int i = 0; i < _matchingHorizontalPosList.Count; ++i)
			{
				if(_matchingHorizontalPosList[i].x < _matchingHorizontalPosList[_maxLeftIndex].x)
				{
					_maxLeftIndex = i;
				}
			}

			Vector2 _specialTilePos = _matchingHorizontalPosList[_maxLeftIndex];
			int _specialTileType = ConvertSpecialTileType_Vertical(tiles[(int)_specialTilePos.x, (int)_specialTilePos.y]);
			tiles[(int)_specialTilePos.x, (int)_specialTilePos.y] = _specialTileType;

			_matchingHorizontalPosList.RemoveAt(_maxLeftIndex);
			for(int i = 0; i < _matchingHorizontalPosList.Count; ++i)
			{
				ClearTile(_matchingHorizontalPosList[i]);
			}

			_specialTileInfos.Add(new SpecialTileMakingInfo(_specialTileType, _specialTilePos, _matchingHorizontalPosList));
		}

		List<Vector2> _matchingVerticalPosList = FindMatch_Vertical(_pos);
		if(3 <= _matchingVerticalPosList.Count) // 기준 위치를 제외한 갯수
		{
			_matchingVerticalPosList.Add(_pos);

			int _maxBottomIndex = 0;
			for(int i = 0; i < _matchingVerticalPosList.Count; ++i)
			{
				if(_matchingVerticalPosList[i].y < _matchingVerticalPosList[_maxBottomIndex].y)
				{
					_maxBottomIndex = i;
				}
			}

			Vector2 _specialTilePos = _matchingVerticalPosList[_maxBottomIndex];
			int _specialTileType = ConvertSpecialTileType_Horizontal(tiles[(int)_specialTilePos.x, (int)_specialTilePos.y]);
			tiles[(int)_specialTilePos.x, (int)_specialTilePos.y] = _specialTileType;

			_matchingVerticalPosList.RemoveAt(_maxBottomIndex);
			for(int i = 0; i < _matchingVerticalPosList.Count; ++i)
			{
				ClearTile(_matchingVerticalPosList[i]);
			}

			_specialTileInfos.Add(new SpecialTileMakingInfo(_specialTileType, _specialTilePos, _matchingVerticalPosList));
		}

		return _specialTileInfos;
	}

	public TileType GetTileType(int _tileID)
	{
		if(_tileID < Constants.SPECIAL_TILE_ID_BASE_VERTICAL)
		{
			return TileType.normal;
		}
		else if(_tileID < Constants.SPECIAL_TILE_ID_BASE_HORIZONTAL)
		{
			return TileType.specialV;
		}
		else
		{
			return TileType.specialH;
		}
	}

	public int GetTileImageIndex(int _tileID)
	{
		if(TileType.normal == GetTileType(_tileID))
		{
			return _tileID;
		}
		else if(TileType.specialV == GetTileType(_tileID))
		{
			return _tileID - Constants.SPECIAL_TILE_ID_BASE_VERTICAL;
		}
		else
		{
			return _tileID - Constants.SPECIAL_TILE_ID_BASE_HORIZONTAL;
		}
	}

	public List<Vector2> MatchSpecialTiles(Vector2 _pos)
	{
		List<Vector2> _clearTiled = new List<Vector2>();

		if(Constants.EMPTY_TILE == GetTileID(_pos))
		{
			return _clearTiled;
		}

		List<Vector2> _matchingCandidates = new List<Vector2>();

		if(IsSpecialType(_pos))
		{
			for(int i = 0; i < adjacentDirections.Length; ++i)
			{
				Vector2 _adjacentPos = _pos + adjacentDirections[i];
				if(false == IsPosValid(_adjacentPos)) { continue; }

				if(IsSpecialType(_adjacentPos))
				{
					_matchingCandidates.Add(_adjacentPos);
				}
			}

			if(0 < _matchingCandidates.Count)
			{
				_matchingCandidates.Add(_pos);
			}
		}

		_matchingCandidates.AddRange(FindMatch_Vertical(_pos));
		_matchingCandidates.AddRange(FindMatch_horizontal(_pos));

		List<Vector2> _specialTiles = new List<Vector2>();
		for(int i = 0; i < _matchingCandidates.Count; ++i)
		{
			if(IsSpecialType(_matchingCandidates[i]))
			{
				_specialTiles.Add(_matchingCandidates[i]);
			}
		}

		for(int i = 0; i < _specialTiles.Count; ++i)
		{
			_clearTiled.AddRange(PlaySpecialTile(_specialTiles[i]));
		}

		for(int i = 0; i < _specialTiles.Count; ++i)
		{
			ClearTile(_specialTiles[i]);
			_clearTiled.Add(_specialTiles[i]);
		}

		return _clearTiled;
	}

	public List<Vector2> ClearAllMatches(Vector2 _pos)
	{
		List<Vector2> _clearTiled = new List<Vector2>();

		if(Constants.EMPTY_TILE == GetTileID(_pos))
		{
			return _clearTiled;
		}

		_clearTiled.AddRange(FindMatch_Vertical(_pos));
		_clearTiled.AddRange(FindMatch_horizontal(_pos));

		if(0 < _clearTiled.Count)
		{
			_clearTiled.Add(_pos);
		}

		for(int i = 0; i < _clearTiled.Count; ++i)
		{
			ClearTile(_clearTiled[i]);
		}

		return _clearTiled;
	}

	public bool IsEmpty(int _x, int _y)
	{
		return tiles[_x, _y] == Constants.EMPTY_TILE;
	}

	public List<int> Fill(int _x, int _startY)
	{
		int _emptyCount = 0;
		for(int _y = _startY; _y < ySize; ++_y)
		{
			if(Constants.EMPTY_TILE == tiles[_x, _y])
			{
				_emptyCount++;
			}
		}

		for(int i = 0; i < _emptyCount; ++i)
		{
			for(int j = _startY; j < ySize; ++j)
			{
				if(ySize - 1 == j)
				{
					tiles[_x, j] = GetNewTileType(_x, j);
				}
				else
				{
					tiles[_x, j] = tiles[_x, j + 1];
					tiles[_x, j + 1] = GetNewTileType(_x, j + 1);
				}
			}
		}

		List<int> _tileTypesFromStartY = new List<int>();
		for(int _y = _startY; _y < ySize; ++_y)
		{
			_tileTypesFromStartY.Add(tiles[_x, _y]);
		}

		return _tileTypesFromStartY;
	}

	List<Vector2> FindMatchBySwap(Vector2 _basePos)
	{
		Dir[] _allDirs = new Dir[] { Dir.left, Dir.right, Dir.up, Dir.down };

		List<Vector2> _hintTilePosList = new List<Vector2>();

		for(int i = 0; i < _allDirs.Length; ++i)
		{
			if(false == CanDragToDir(_basePos, _allDirs[i]))
			{
				continue;
			}

			// Swap 시뮬레이션
			Vector2 _swapTargetPos = GetTilePosAboutDir(_basePos, _allDirs[i]);
			Swap(_basePos, _swapTargetPos);

			//
			_hintTilePosList = FindMatch_horizontal(_swapTargetPos);
			if(0 < _hintTilePosList.Count)
			{
				_hintTilePosList.Add(_basePos);

				// 원래대로 복원
				Swap(_basePos, _swapTargetPos);

				break;
			}

			_hintTilePosList = FindMatch_Vertical(_swapTargetPos);
			if(0 < _hintTilePosList.Count)
			{
				_hintTilePosList.Add(_basePos);

				// 원래대로 복원
				Swap(_basePos, _swapTargetPos);

				break;
			}

			// 원래대로 복원
			Swap(_basePos, _swapTargetPos);
		}

		return _hintTilePosList;
	}

	TileType GetTileType(Vector2 _pos)
	{
		return GetTileType(GetTileID(_pos));
	}

	bool IsPosValid(Vector2 _pos)
	{
		return (0 <= _pos.x && _pos.x < xSize) && (0 <= _pos.y && _pos.y < ySize);
	}

	int GetTileID(Vector2 _pos)
	{
		return tiles[(int)_pos.x, (int)_pos.y];
	}

	bool IsSpecialType(Vector2 _pos)
	{
		return IsSpecialType(GetTileID(_pos));
	}

	bool IsSpecialType(int _tileID)
	{
		return GetTileType(_tileID) == TileType.specialH || GetTileType(_tileID) == TileType.specialV;
	}

	List<Vector2> PlaySpecialTile(Vector2 _pos)
	{
		List<Vector2> _clearedTilePosList = new List<Vector2>();

		int _id = tiles[(int)_pos.x, (int)_pos.y];
		TileType _tileType = GetTileType(_id);
		if(TileType.specialH == _tileType)
		{
			for(int _x = 0; _x < xSize; ++_x)
			{
				Vector2 _clearPos = new Vector2(_x, (int)_pos.y);
				if(IsSpecialType(_clearPos)) { continue; }

				ClearTile(_clearPos);
				_clearedTilePosList.Add(_clearPos);
			}
		}
		else if(TileType.specialV == _tileType)
		{
			for(int _y = 0; _y < ySize; ++_y)
			{
				Vector2 _clearPos = new Vector2((int)_pos.x, _y);
				if(IsSpecialType(_clearPos)) { continue; }

				ClearTile(_clearPos);
				_clearedTilePosList.Add(_clearPos);
			}
		}

		return _clearedTilePosList;
	}	

	int ConvertSpecialTileType_Vertical(int _normalTileType)
	{
		return _normalTileType + Constants.SPECIAL_TILE_ID_BASE_VERTICAL;
	}

	int ConvertSpecialTileType_Horizontal(int _normalTileType)
	{
		return _normalTileType + Constants.SPECIAL_TILE_ID_BASE_HORIZONTAL;
	}

	void ClearTile(Vector2 _pos)
	{
		tiles[(int)_pos.x, (int)_pos.y] = Constants.EMPTY_TILE;
	}

	List<Vector2> FindMatch_Vertical(Vector2 _basePos)
	{
		Vector2[] _dirs = new Vector2[2] { Vector2.up, Vector2.down };
		return FindMatchAll(_basePos, _dirs);
	}

	List<Vector2> FindMatch_horizontal(Vector2 _basePos)
	{
		Vector2[] _dirs = new Vector2[2] { Vector2.left, Vector2.right };
		return FindMatchAll(_basePos, _dirs);
	}

	List<Vector2> FindMatchAll(Vector2 _basePos, Vector2[] _dirs)
	{
		List<Vector2> _tempMatchingTiles = new List<Vector2>();

		for(int i = 0; i < _dirs.Length; ++i)
		{
			_tempMatchingTiles.AddRange(FindMatch(_basePos, _dirs[i]));
		}

		List<Vector2> _matchingTiles = new List<Vector2>();

		if(2 <= _tempMatchingTiles.Count)
		{
			_matchingTiles.AddRange(_tempMatchingTiles);
		}

		return _matchingTiles;
	}

	List<Vector2> ClearMatch(Vector2 _pos, Vector2[] _dirs)
	{
		List<Vector2> _matchingTiles = new List<Vector2>();
		for(int i = 0; i < _dirs.Length; ++i)
		{
			_matchingTiles.AddRange(FindMatch(_pos, _dirs[i]));
		}

		List<Vector2> _clearedTiled = new List<Vector2>();

		if(2 <= _matchingTiles.Count)
		{
			_clearedTiled.AddRange(_matchingTiles);
		}

		for(int i = 0; i < _clearedTiled.Count; ++i)
		{
			ClearTile(_clearedTiled[i]);
		}

		return _clearedTiled;
	}

	List<Vector2> FindMatch(Vector2 _pos, Vector2 _dir)
	{
		List<Vector2> _matchingTiles = new List<Vector2>();

		Vector2 _basePos = _pos;

		while(IsSameAt(_basePos, _dir))
		{
			_basePos += _dir;
			_matchingTiles.Add(_basePos);
		}

		return _matchingTiles;
	}

	bool IsSameAt(Vector2 _basePos, Vector2 _dir)
	{
		Vector2 _targetPos = _basePos + _dir;
		if(_targetPos.x < 0 || xSize - 1 < _targetPos.x ||
		   _targetPos.y < 0 || ySize - 1 < _targetPos.y)
		{
			return false;
		}

		return GetTileImageIndex(tiles[(int)_targetPos.x, (int)_targetPos.y]) == GetTileImageIndex(tiles[(int)_basePos.x, (int)_basePos.y]);
	}

	int GetNewTileType(int _x, int _y)
	{
		List<int> _possibleTiles = new List<int>();
		_possibleTiles.AddRange(allTileTypes);

		if(0 < _x)
		{
			_possibleTiles.Remove(tiles[_x - 1, _y]);
		}
		if(_x < xSize - 1)
		{
			_possibleTiles.Remove(tiles[_x + 1, _y]);
		}
		if(0 < _y)
		{
			_possibleTiles.Remove(tiles[_x, _y - 1]);
		}

		return _possibleTiles[UnityEngine.Random.Range(0, _possibleTiles.Count)];
	}
}
