﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Alarm : MonoBehaviour
{
	bool _startTimer = false;
	float _targetTime = 0;
	float _elapsedTime = 0;

	Action onAlarm = () => { };

	public void StartTimer(float _time, Action _onAlarm)
	{
		_targetTime = _time;
		onAlarm = _onAlarm;

        _startTimer = true;
		_elapsedTime = 0;
    }

	public void StopTimer()
	{
		_startTimer = false;
		_elapsedTime = 0;
    }

	void Update()
	{
		if(false == _startTimer) { return; }

		_elapsedTime += Time.deltaTime;
		if(_targetTime <= _elapsedTime)
		{
			onAlarm();
			StopTimer();
        }
    }
}
