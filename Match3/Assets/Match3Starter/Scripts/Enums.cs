﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CandyCrushFake
{
	public enum Dir
	{
		up = 0,
		down,
		left,
		right,
	}

	public enum GameEventType
	{
		CLICK_REPLAY = 0,
		CLICK_SKILL,
		TILE_MOVE,
		GET_SCORE,
	}

	public enum TileType
	{
		normal = 0,
		specialV,
		specialH,
	}
}

