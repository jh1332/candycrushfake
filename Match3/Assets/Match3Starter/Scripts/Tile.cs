﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tile : MonoBehaviour
{
	int id = -1;
	SpriteRenderer render;
	Board board = null;

	Vector2 touchStartPos;
	float dragCheckRange = 0;

	public int ID
	{
		get { return id; }
	}

	public void Init(int _id, Board _board)
	{
		id = _id;
		board = _board;
	}

	public void SetTileImage(Sprite _sprite)
	{
		render.sprite = _sprite;
	}

	public bool IsEmptyTile()
	{
		return null == render.sprite;
	}

	void Awake()
	{
		render = GetComponent<SpriteRenderer>();
		dragCheckRange = render.bounds.size.x * 0.5f;
	}

	void OnEnable()
	{
		EasyTouch.On_TouchStart += On_TouchStart;
		EasyTouch.On_TouchDown += On_TouchDown;
	}

	void OnDisable()
	{
		EasyTouch.On_TouchStart -= On_TouchStart;
		EasyTouch.On_TouchDown -= On_TouchDown;
	}

	void On_TouchStart(Gesture _g)
	{
		if(_g.pickObject != gameObject) { return; }

		touchStartPos = GetTouchPoint();
	}

	void On_TouchDown(Gesture _g)
	{
		if(_g.pickObject != gameObject) { return; }

		Vector2 _curPos = GetTouchPoint();
		if(dragCheckRange <= Mathf.Abs(touchStartPos.x - _curPos.x))
		{
			if(touchStartPos.x <= _curPos.x)
			{
				board.OnDragTile(this, CandyCrushFake.Dir.right);
			}
			else
			{
				board.OnDragTile(this, CandyCrushFake.Dir.left);
			}
		}
		else if(dragCheckRange <= Mathf.Abs(touchStartPos.y - _curPos.y))
		{
			if(touchStartPos.y <= _curPos.y)
			{
				board.OnDragTile(this, CandyCrushFake.Dir.up);
			}
			else
			{
				board.OnDragTile(this, CandyCrushFake.Dir.down);
			}
		}
	}

	Vector3 GetTouchPoint()
	{
		return Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
	}
}