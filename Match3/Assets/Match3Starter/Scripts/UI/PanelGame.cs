﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CandyCrushFake;
using Unit5.EventBusSystem;

public class PanelGame : MonoBehaviour
{
	[SerializeField] UIButton btnReplay = null;
	[SerializeField] UILabel lblMove = null;
	[SerializeField] UILabel lblScore = null;

	public void Set(int _move, int _score)
	{
		SetMove(_move);
		SetScore(_score);
    }

	void Awake()
	{
		EventDelegate.Set(btnReplay.onClick, OnClick_Replay);

		EventBus.Subscribe<GameEventType>(OnGameEvent);
	}

	void SetMove(int _move)
	{
		lblMove.text = _move.ToString();
    }

	void SetScore(int _score)
	{
		lblScore.text = _score.ToString("N0");
    }

	void OnClick_Replay()
	{
		EventBus.Dispatch(GameEventType.CLICK_REPLAY);
	}

	void OnClick_Skill()
	{
		EventBus.Dispatch(GameEventType.CLICK_SKILL);
	}

	void OnGameEvent(GameEventType _e, object _param)
	{
		switch(_e)
		{
			case GameEventType.TILE_MOVE:
				SetMove((int)_param);
                break;

			case GameEventType.GET_SCORE:
				SetScore((int)_param);
				break;
		}
	}
}
