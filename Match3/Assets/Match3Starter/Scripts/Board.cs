﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Board : MonoBehaviour
{
	static int TILE_ID_BASE = 0;

	Tile[,] tiles;

	GameControl gameControl = null;

	int xSize;
	int ySize;
	float yOffset = 0;

	List<Sprite> tileImgs = null;
	List<Sprite> specialVerticalTileImgs = null;
	List<Sprite> specialHorizontalTileImgs = null;

	bool swapping = false;

	int swapTweenEndNum = 0;
	Action onTweenSwapEnd = () => { };
	Action onTweenFillEnd = () => { };

	float tileScale = 0;
	float tileScaleHint = 0;
	List<GameObject> hintTiles = new List<GameObject>();

	public void Init(GameControl _gameControl, List<Sprite> _tileImgs, List<Sprite> _specialVerticalTileImgs, List<Sprite> _specialHorizontalTileImg)
	{
		gameControl = _gameControl;
		tileImgs = _tileImgs;
		specialVerticalTileImgs = _specialVerticalTileImgs;
		specialHorizontalTileImgs = _specialHorizontalTileImg;
	}

	public void CreateBoard(GameObject _tilePrefab, int[,] _boardTable, int _xSize, int _ySize)
	{
		Clear();

		xSize = _xSize;
		ySize = _ySize;

		Vector2 offset = _tilePrefab.GetComponent<SpriteRenderer>().bounds.size;
		float xOffset = offset.x;
		yOffset = offset.y;

		tiles = new Tile[xSize, ySize];

		float startX = transform.position.x;
		float startY = transform.position.y;

		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				Tile newTile = Instantiate(_tilePrefab, new Vector3(startX + (xOffset * x), startY + (yOffset * y), 0), Quaternion.identity).GetComponent<Tile>();
				newTile.GetComponent<Tile>().Init(TILE_ID_BASE++, this);
				tiles[x, y] = newTile;
				newTile.transform.parent = transform;

				int _tileType= _boardTable[x, y];
				newTile.SetTileImage(tileImgs[_tileType]);
			}
		}

		tileScale = tiles[0, 0].transform.localScale.x;
		tileScaleHint = tileScale * 1.2f;
    }

	public void Swap(Vector2 _aPos, Vector2 _bPos, Action _onEnd)
	{
		onTweenSwapEnd = _onEnd;

		Tile _aTile = tiles[(int)_aPos.x, (int)_aPos.y];
		Tile _bTile = tiles[(int)_bPos.x, (int)_bPos.y];

		Vector3 _aTargetPos = _bTile.transform.position;
		Vector3 _bTargetPos = _aTile.transform.position;

		swapTweenEndNum = 2;

		iTween.MoveTo(_aTile.gameObject,
			iTween.Hash("name", "SwapTween",
					  "position", _aTargetPos,
					  "time", 0.3f,
					  "easetype", iTween.EaseType.linear,
					  "oncompletetarget", gameObject,
					  "oncomplete", "OnTweenSwapEnd"));

		iTween.MoveTo(_bTile.gameObject,
			iTween.Hash("name", "SwapTween",
					  "position", _bTargetPos,
					  "time", 0.3f,
					  "easetype", iTween.EaseType.linear,
					  "oncompletetarget", gameObject,
					  "oncomplete", "OnTweenSwapEnd"));

		tiles[(int)_aPos.x, (int)_aPos.y] = _bTile;
		tiles[(int)_bPos.x, (int)_bPos.y] = _aTile;

		SFXManager.instance.PlaySFX(Clip.Swap);
	}

	public void NoSwap(Vector2 _aPos, Vector2 _bPos, Action _onEnd)
	{
		Swap(_aPos, _bPos, () =>
		{
			Swap(_aPos, _bPos, ()=> { _onEnd(); });
		});
	}

	public void MakeSpecialTile(int _tileID, Vector2 _pos)
	{
		Sprite _img = null;
		int _imgIndex = gameControl.GetTileImageIndex(_tileID);
		if(gameControl.GetTileType(_tileID) == CandyCrushFake.TileType.specialH)
		{
			_img = specialHorizontalTileImgs[_imgIndex];
		}
		else
		{
			_img = specialVerticalTileImgs[_imgIndex];
		}

		tiles[(int)_pos.x, (int)_pos.y].SetTileImage(_img);
	}

	public void ClearTiles(List<Vector2> _targetPosList, Action _onEnd)
	{
		for(int i = 0; i < _targetPosList.Count; ++i)
		{
			tiles[(int)_targetPosList[i].x, (int)_targetPosList[i].y].SetTileImage(null);
		}

		if(0 < _targetPosList.Count)
		{
			SFXManager.instance.PlaySFX(Clip.Clear);
		}

		_onEnd();
	}

	public void FillTiles(int _x, int _startY, List<int> _tileTypesFromStartY, Action _onEnd)
	{
		onTweenFillEnd = _onEnd;

		int _emptyCount = 0;
		for(int _y = _startY; _y < ySize; ++_y)
		{
			if(tiles[_x, _y].IsEmptyTile())
			{
				_emptyCount++;
			}
		}

		float startY = transform.position.y;

		for(int i = 0, _y = _startY; i < _tileTypesFromStartY.Count; ++i, ++_y)
		{
			tiles[_x, _y].SetTileImage(GetTileImage(_tileTypesFromStartY[i]));

			Vector3 _targetPos = tiles[_x, _y].transform.position;
			tiles[_x, _y].transform.position = new Vector3(_targetPos.x, _targetPos.y + (yOffset * _emptyCount), _targetPos.z);

			if(_tileTypesFromStartY.Count - 1 == i)
			{
				iTween.MoveTo(tiles[_x, _y].gameObject,
					iTween.Hash("name", "FillTween",
							  "position", _targetPos,
							  "time", 0.5f,
							  "easetype", iTween.EaseType.easeOutBounce,
							  "oncompletetarget", gameObject,
							  "oncomplete", "OnFillTweenEnd"));
			}
			else
			{
				iTween.MoveTo(tiles[_x, _y].gameObject,
					iTween.Hash("name", "FillTween",
							  "position", _targetPos,
							  "time", 0.5f,
							  "easetype", iTween.EaseType.easeOutBounce));
			}
		}
	}

	public void ShowHint(List<Vector2> _hintTileIndexes)
	{
		for(int i = 0; i < _hintTileIndexes.Count; ++i)
		{
			GameObject _tile = tiles[(int)_hintTileIndexes[i].x, (int)_hintTileIndexes[i].y].gameObject;

			iTween.ScaleTo(_tile,
				iTween.Hash("name", "HintTween",
						  "x", tileScaleHint,
						  "y", tileScaleHint,
						  "time", 0.5f,
						  "easetype", iTween.EaseType.linear,
						  "looptype", iTween.LoopType.loop));

			hintTiles.Add(_tile);
		}
	}

	public void HideHint()
	{
		iTween.StopByName("HintTween");

		for(int i = 0; i < hintTiles.Count; ++i)
		{
			hintTiles[i].transform.localScale = new Vector3(tileScale, tileScale, hintTiles[i].transform.localScale.z);
		}

		hintTiles.Clear();
	}

	public void OnDragTile(Tile _tile, CandyCrushFake.Dir _dir)
	{
		Vector2 _tileIndex = GetTilePos(_tile);

		if(false == gameControl.CanDragToDir(_tileIndex, _dir)) { return; }
		if(swapping) { return; }

		swapping = true;

		gameControl.TrySwap(_tileIndex, _dir, () =>
		{
			swapping = false;
		});
	}

	void Clear()
	{
		swapping = false;
		swapTweenEndNum = 0;
		hintTiles.Clear();

		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				Destroy(tiles[x, y].gameObject);
			}
		}
    }

	void OnTweenSwapEnd()
	{
		swapTweenEndNum--;
		if(0 == swapTweenEndNum)
		{
			onTweenSwapEnd();
		}
	}

	Sprite GetTileImage(int _tileID)
	{
		Sprite _sprite = null;

		int _imgIndex = gameControl.GetTileImageIndex(_tileID);

		CandyCrushFake.TileType _tileType = gameControl.GetTileType(_tileID);
		if(CandyCrushFake.TileType.specialH == _tileType)
		{
			_sprite = specialHorizontalTileImgs[_imgIndex];
		}
		else if(CandyCrushFake.TileType.specialV == _tileType)
		{
			_sprite = specialVerticalTileImgs[_imgIndex];
		}
		else
		{
			_sprite = tileImgs[_imgIndex];
		}

		return _sprite;
	}

	void OnFillTweenEnd()
	{
		onTweenFillEnd();
	}

	Vector2 GetTilePos(Tile _tile)
	{
		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				if(tiles[x, y].ID == _tile.ID)
				{
					return new Vector2(x, y);
				}
			}
		}

		return Vector2.zero;
	}
}
