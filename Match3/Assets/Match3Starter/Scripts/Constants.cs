﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CandyCrushFake
{
	public static class Constants
	{
		public const int EMPTY_TILE = -1;
		public const int SPECIAL_TILE_ID_BASE_VERTICAL = 100;
		public const int SPECIAL_TILE_ID_BASE_HORIZONTAL = 200;
	}
}
