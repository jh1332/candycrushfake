﻿using UnityEngine;

#if UNITY_WEBPLAYER && !UNITY_EDITOR
using D = WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

//----------
public class SingletonMB<T> : MonoBehaviour where T : MonoBehaviour
{
	
	//----------
	static protected T __instance;
	
	//----------
	static public T instance
	{
		get
		{
			if(__instance == null)
			{
				__instance = (T)FindObjectOfType(typeof(T));
				
				if(FindObjectsOfType(typeof(T)).Length > 1)
				{
					Debug.LogError("[SingletonMB] Something went really wrong  - there should never be more than 1 singleton! Reopening the scene might fix it.");
					return __instance;
				}
				
				if(__instance == null)
				{
					GameObject go = new GameObject("_" + typeof(T).ToString());
					__instance = go.AddComponent<T>();
					
					#if UNITY_EDITOR
					if(Application.isPlaying)
					{
						DontDestroyOnLoad(go);
					}
					#else
					DontDestroyOnLoad(go);
					#endif
				}
			}
			return __instance;
		}
	}

	static public bool IsExistInstance()
	{
		return (__instance != null);
	}
}
