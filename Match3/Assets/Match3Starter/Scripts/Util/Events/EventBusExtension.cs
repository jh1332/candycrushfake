﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;

#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace Unit5.EventBusSystem
{

//----------
static public class EventBusExtension
{
	
	//----------
	static public void BindUntilDisable<T>(this MonoBehaviour mono, Action<T> onEvent)
	{
		GetOrAddComponent<T, EventBusDisableSubscriber>(mono, EventBusInternal.DefaultTag, onEvent);
	}
	static public void BindUntilDisable<T>(this MonoBehaviour mono, object tag, Action<T> onEvent)
	{
		GetOrAddComponent<T, EventBusDisableSubscriber>(mono, tag, onEvent);
	}
	
	static public void BindUntilDisable<T>(this MonoBehaviour mono, Action<T, object> onEvent)
	{
		GetOrAddComponent<T, EventBusDisableSubscriber>(mono, EventBusInternal.DefaultTag, onEvent);
	}
	static public void BindUntilDisable<T>(this MonoBehaviour mono, object tag, Action<T, object> onEvent)
	{
		GetOrAddComponent<T, EventBusDisableSubscriber>(mono, tag, onEvent);
	}
	
	//----------
	static public void BindUntilDestroy<T>(this MonoBehaviour mono, Action<T> onEvent)
	{
		GetOrAddComponent<T, EventBusDestroySubscriber>(mono, EventBusInternal.DefaultTag, onEvent);
	}
	static public void BindUntilDestroy<T>(this MonoBehaviour mono, object tag, Action<T> onEvent)
	{
		GetOrAddComponent<T, EventBusDestroySubscriber>(mono, tag, onEvent);
	}
	
	static public void BindUntilDestroy<T>(this MonoBehaviour mono, Action<T, object> onEvent)
	{
		GetOrAddComponent<T, EventBusDestroySubscriber>(mono, EventBusInternal.DefaultTag, onEvent);
	}
	static public void BindUntilDestroy<T>(this MonoBehaviour mono, object tag, Action<T, object> onEvent)
	{
		GetOrAddComponent<T, EventBusDestroySubscriber>(mono, tag, onEvent);
	}
	
	//----------
	static void GetOrAddComponent<T, S>(MonoBehaviour mono, object tag, Action<T> onEvent) where S : EventBusSubscriberBase
	{
		var component = mono.GetComponent<S>();
		if(null == component)component = mono.gameObject.AddComponent<S>();
		
		component.SetSubscribeCaller(tag, onEvent);
	}
	
	static void GetOrAddComponent<T, S>(MonoBehaviour mono, object tag, Action<T, object> onEvent) where S : EventBusSubscriberBase
	{
		var component = mono.GetComponent<S>();
		if(null == component)component = mono.gameObject.AddComponent<S>();
		
		component.SetSubscribeCaller(tag, onEvent);
	}
}

}