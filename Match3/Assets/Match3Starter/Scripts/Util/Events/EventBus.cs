﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;

#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace Unit5.EventBusSystem
{

//----------
static public class EventBus
{
	
	//----------
	static public void Clear()
	{
		EventBusInternal.instance.Clear();
	}
	
	//----------
	static public void Subscribe<T>(Action<T> eventCallback)
	{
		EventBusInternal.instance.Subscribe<T>(eventCallback);
	}
	static public void Subscribe<T>(object tag, Action<T> eventCallback)
	{
		EventBusInternal.instance.Subscribe<T>(tag, eventCallback);
	}
	
	static public void Subscribe<T>(Action<T, object> eventCallback)
	{
		EventBusInternal.instance.Subscribe<T>(eventCallback);
	}
	static public void Subscribe<T>(object tag, Action<T, object> eventCallback)
	{
		EventBusInternal.instance.Subscribe<T>(tag, eventCallback);
	}
	
	//----------
	static public void Unsubscribe<T>(Action<T> eventCallback)
	{
		EventBusInternal.instance.Unsubscribe<T>(EventBusInternal.DefaultTag, eventCallback);
	}
	static public void Unsubscribe<T>(object tag, Action<T> eventCallback)
	{
		EventBusInternal.instance.Unsubscribe<T>(tag, eventCallback);
	}
	
	static public void Unsubscribe<T>(Action<T, object> eventCallback)
	{
		EventBusInternal.instance.Unsubscribe<T>(EventBusInternal.DefaultTag, eventCallback);
	}
	static public void Unsubscribe<T>(object tag, Action<T, object> eventCallback)
	{
		EventBusInternal.instance.Unsubscribe<T>(tag, eventCallback);
	}
	
	//----------
	static public void Dispatch<T>(T action)
	{
		EventBusInternal.instance.Dispatch(EventBusInternal.DefaultTag, action, null);
	}
	static public void Dispatch<T>(T action, object param)
	{
		EventBusInternal.instance.Dispatch(EventBusInternal.DefaultTag, action, param);
	}
	static public void Dispatch<T>(object tag, T action, object param)
	{
		EventBusInternal.instance.Dispatch(tag, action, param);
	}
}

}