﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;

#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace Unit5.EventBusSystem
{

//----------
public class EventBusSubscriberBase : MonoBehaviour
{
	
	//----------
	protected Action<bool> subscribeCaller;
	
	//----------
	public void SetSubscribeCaller<T>(object tag, Action<T> onEvent)
	{
		subscribeCaller = (bool active) => {
			if(active)
			{
				EventBusInternal.instance.Subscribe(tag, onEvent);
			}
			else
			{
				if(EventBusInternal.IsExistInstance())
				{
					EventBusInternal.instance.Unsubscribe(tag, onEvent);
				}
			}
		};
		
		subscribeCaller(true);
	}
	
	public void SetSubscribeCaller<T>(object tag, Action<T, object> onEvent)
	{
		subscribeCaller = (bool active) => {
			if(active)
			{
				EventBusInternal.instance.Subscribe(tag, onEvent);
			}
			else
			{
				if(EventBusInternal.IsExistInstance())
				{
					EventBusInternal.instance.Unsubscribe(tag, onEvent);
				}
			}
		};
		
		subscribeCaller(true);
	}
}

}