﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;

#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace Unit5.EventBusSystem
{

//----------
class DictionaryKey
{
	public Type Type;
	public object Tag;
	
	public DictionaryKey(object tag, Type type)
	{
		this.Tag = tag;
		this.Type = type;
	}
	
	public override int GetHashCode()
	{
		return this.Tag.GetHashCode() ^ this.Type.GetHashCode();
	}
	public override bool Equals(object obj)
	{
		if(obj is DictionaryKey)
		{
			var key = (DictionaryKey) obj;
			return this.Tag.Equals(key.Tag) && this.Type.Equals(key.Type);
		}
		return false;
	}
}

}