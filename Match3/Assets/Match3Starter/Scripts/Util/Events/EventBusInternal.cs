﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;

#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace Unit5.EventBusSystem
{

//----------
public delegate void OnEventWrapper(object type);
public delegate void OnEventWithParamWrapper(object type, object param);

//----------
public class EventBusInternal : SingletonMB<EventBusInternal>
{
	
	//----------
	public const string DefaultTag = "default";
	
	//----------
	Dictionary<DictionaryKey, Dictionary<int, OnEventWrapper>> eventObservers = new Dictionary<DictionaryKey, Dictionary<int, OnEventWrapper>>();
	Dictionary<DictionaryKey, Dictionary<int, OnEventWithParamWrapper>> eventWithParamObservers = new Dictionary<DictionaryKey, Dictionary<int, OnEventWithParamWrapper>>();
	
	//----------
	public void Clear()
	{
		eventObservers.Clear();
		eventWithParamObservers.Clear();
	}
	
	//----------
	public void Subscribe<T>(Action<T> eventCallback)
	{
		this.Subscribe(DefaultTag, eventCallback);
	}
	public void Subscribe<T>(object tag, Action<T> eventCallback)
	{
		var key = new DictionaryKey(tag, typeof(T));
		if(!eventObservers.ContainsKey(key))
		{
			eventObservers[key] = new Dictionary<int, OnEventWrapper>();
		}
		eventObservers[key][eventCallback.GetHashCode()] = (object type) => {
			eventCallback((T)type);
		};
	}
	
	public void Subscribe<T>(Action<T, object> eventCallback)
	{
		this.Subscribe(DefaultTag, eventCallback);
	}
	public void Subscribe<T>(object tag, Action<T, object> eventCallback)
	{
		var key = new DictionaryKey(tag, typeof(T));
		if(!eventWithParamObservers.ContainsKey(key))
		{
			eventWithParamObservers[key] = new Dictionary<int, OnEventWithParamWrapper>();
		}
		eventWithParamObservers[key][eventCallback.GetHashCode()] = (object type, object param) => {
			eventCallback((T)type, param);
		};
	}
	
	//----------
	public void Unsubscribe<T>(Action<T> eventCallback)
	{
		this.Unsubscribe(DefaultTag, eventCallback);
	}
	public void Unsubscribe<T>(object tag, Action<T> eventCallback)
	{
		var key = new DictionaryKey(tag, typeof(T));
		if(eventObservers[key] != null)
		{
			eventObservers[key].Remove(eventCallback.GetHashCode());
		}
	}
	
	public void Unsubscribe<T>(Action<T, object> eventCallback)
	{
		this.Unsubscribe(DefaultTag, eventCallback);
	}
	public void Unsubscribe<T>(object tag, Action<T, object> eventCallback)
	{
		var key = new DictionaryKey(tag, typeof(T));
		if(eventWithParamObservers[key] != null)
		{
			eventWithParamObservers[key].Remove(eventCallback.GetHashCode());
		}
	}
	
	//----------
	public void Dispatch<T>(T action)
	{
		this.Dispatch(DefaultTag, action, null);
	}
	public void Dispatch<T>(T action, object param)
	{
		this.Dispatch(DefaultTag, action, param);
	}
	public void Dispatch<T>(object tag, T action, object param)
	{
		bool fired = false;
		
		var key = new DictionaryKey(tag, typeof(T));
		if(eventObservers.ContainsKey(key))
		{
			fired = true;
			
			foreach(var caller in eventObservers[key].Values)
			{
				caller(action);
			}
		}
		if(param != null && eventWithParamObservers.ContainsKey(key))
		{
			fired = true;
			
			foreach(var caller in eventWithParamObservers[key].Values)
			{
				caller(action, param);
			}
		}
		
		if(!fired)
		{
			Debug.LogWarning(string.Format("[UNIT5::EventBusInternal] {0}:: EventBus.Dispatch failed to send: (tag:{1}, action:{2}, param:{3})", "OnValueChanged", tag, action, param));
		}
	}
}

}