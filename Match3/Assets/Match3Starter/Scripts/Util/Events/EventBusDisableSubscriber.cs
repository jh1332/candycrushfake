﻿using UnityEngine;
using UnityObject = UnityEngine.Object;
using UnityRandom = UnityEngine.Random;

#if UNITY_WEBGL && !UNITY_EDITOR
using D = Unit5.WebPlayerDebug;
#else
using D = UnityEngine.Debug;
#endif

using System;
using System.Collections;
using System.Collections.Generic;

//----------
namespace Unit5.EventBusSystem
{

//----------
public class EventBusDisableSubscriber : EventBusSubscriberBase
{
	
	//----------
	void OnEnable()
	{
		if(subscribeCaller != null)subscribeCaller(true);
	}
	void OnDisable()
	{
		if(subscribeCaller != null)subscribeCaller(false);
	}
}

}