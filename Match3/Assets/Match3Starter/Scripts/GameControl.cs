﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

using CandyCrushFake;
using Unit5.EventBusSystem;

public class GameControl : MonoBehaviour
{
	class TileClearAndFillAct
	{
		class FillInfo
		{
			public int x = 0;
			public int startY = 0;
			public List<int> tileTypesFromStartY = null;

			public FillInfo(int _x, int _startY, List<int> _tileTypesFromStartY)
			{
				x = _x;
				startY = _startY;
				tileTypesFromStartY = _tileTypesFromStartY;
			}
		}

		Board board = null;
		List<Vector2> clearTargetPosList = new List<Vector2>();
		List<FillInfo> fillInfos = new List<FillInfo>();
		List<BoardData.SpecialTileMakingInfo> _specialTileMakingInfos = new List<BoardData.SpecialTileMakingInfo>();

		public TileClearAndFillAct(Board _board)
		{
			board = _board;
		}

		public void SetClearInfo(List<Vector2> _clearTargetPosList)
		{
			clearTargetPosList = _clearTargetPosList;
		}

		public void AddFillInfo(int _x, int _startY, List<int> _tileTypesFromStartY)
		{
			fillInfos.Add(new FillInfo(_x, _startY, _tileTypesFromStartY));
		}

		public void AddSpecialTileInfo(BoardData.SpecialTileMakingInfo _info)
		{
			_specialTileMakingInfos.Add(_info);
		}

		public void Play(Action _onEnd)
		{
			board.ClearTiles(clearTargetPosList, () => 
			{
				for(int i = 0; i < _specialTileMakingInfos.Count; ++i)
				{
					board.MakeSpecialTile(_specialTileMakingInfos[i].type, _specialTileMakingInfos[i].pos);
				}

				Action _onFillActEnd = () => { };

				for(int i = 0; i < fillInfos.Count; ++i)
				{
					if(fillInfos.Count - 1 == i)
					{
						_onFillActEnd = () => { _onEnd(); };
					}
					else
					{
						_onFillActEnd = () => { };
					}

					board.FillTiles(fillInfos[i].x, fillInfos[i].startY, fillInfos[i].tileTypesFromStartY, _onFillActEnd);
				}
			});
		}
	}

	readonly int MOVE_MAX = 99;
	readonly float HINT_TIME = 2;

	[SerializeField] Board board = null;
	[SerializeField] PanelGame panelGame = null;
	[SerializeField] Alarm hintAlarm = null;
	[SerializeField] List<Sprite> tiles = new List<Sprite>();
	[SerializeField] List<Sprite> specialVerticalTiles = new List<Sprite>();
	[SerializeField] List<Sprite> specialHorizontalTiles = new List<Sprite>();
	[SerializeField] GameObject tilePrefab = null;
	[SerializeField] int xSize = 0;
	[SerializeField] int ySize = 0;

	BoardData boardData = null;
	int move = 0;
	int score = 0;

	List<TileClearAndFillAct> clearAndFillActList = new List<TileClearAndFillAct>();

	Action onEnd_TrySwapping = () => { };

	public void TrySwap(Vector2 _baseTilePos, Dir _dir, Action _onEnd)
	{
		StopHint();

		Vector2 _targetTilePos = boardData.GetTilePosAboutDir(_baseTilePos, _dir);
		if(boardData.CanMatchBySwap(_baseTilePos, _dir)) // 타일의 해당 방향으로 Swap했을 때 매칭되는 것이 있으면
		{
			onEnd_TrySwapping = _onEnd;

			// Swap
			boardData.Swap(_baseTilePos, _targetTilePos);

			// Swap 연출
			board.Swap(_baseTilePos, _targetTilePos, () =>
			{
				ClearMatchAndFill(_baseTilePos);

				for(int x = 0; x < xSize; x++)
				{
					for(int y = 0; y < ySize; y++)
					{
						ClearMatchAndFill(new Vector2(x, y));
					}
				}

				//
				ClearMatchAndFill(_targetTilePos);

				for(int x = 0; x < xSize; x++)
				{
					for(int y = 0; y < ySize; y++)
					{
						ClearMatchAndFill(new Vector2(x, y));
					}
				}

				StopCoroutine("PlayClearAndFillAct_Coroutine");
				StartCoroutine("PlayClearAndFillAct_Coroutine");
			});

			//
			EventBus.Dispatch(GameEventType.TILE_MOVE, --move);
		}
		else
		{
			// No Swap 연출
			board.NoSwap(_baseTilePos, _targetTilePos, () => 
			{
				_onEnd();
			});
		}
	}

	public bool CanDragToDir(Vector2 _basePos, Dir _dir)
	{
		return boardData.CanDragToDir(_basePos, _dir);
	}

	public TileType GetTileType(int _tileID)
	{
		return boardData.GetTileType(_tileID);
	}

	public int GetTileImageIndex(int _tileID)
	{
		return boardData.GetTileImageIndex(_tileID);
	}

	IEnumerator PlayClearAndFillAct_Coroutine()
	{
		bool _stepEnd = false;

		for(int i = 0; i < clearAndFillActList.Count; ++i)
		{
			clearAndFillActList[i].Play(() => 
			{
				score += 50;
				EventBus.Dispatch(GameEventType.GET_SCORE, score);

				_stepEnd = true;
			});

			while(false == _stepEnd)
			{
				yield return null;
			}

			_stepEnd = false;
		}

		clearAndFillActList.Clear();
		onEnd_TrySwapping();
		onEnd_TrySwapping = () => { };
	}

	void ClearSpecialMatchAndFill()
	{
		List<Vector2> _clearTargetPosList = new List<Vector2>();

		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				_clearTargetPosList.AddRange(boardData.MatchSpecialTiles(new Vector2(x, y)));
			}
		}

		if(0 < _clearTargetPosList.Count)
		{
			TileClearAndFillAct _clearAndFillAct = new TileClearAndFillAct(board);
			_clearAndFillAct.SetClearInfo(_clearTargetPosList);

			// 빈 타일들을 찾아 채우기
			for(int x = 0; x < xSize; x++)
			{
				for(int y = 0; y < ySize; y++)
				{
					if(boardData.IsEmpty(x, y))
					{
						_clearAndFillAct.AddFillInfo(x, y, boardData.Fill(x, y));
					}
				}
			}

			clearAndFillActList.Add(_clearAndFillAct);
		}
	}

	void ClearMatchAndFill(Vector2 _pos)
	{
		ClearSpecialMatchAndFill();

		TileClearAndFillAct _clearAndFillAct = new TileClearAndFillAct(board);
		List<Vector2> _clearTargetPosList = new List<Vector2>();

		//
		List<BoardData.SpecialTileMakingInfo> _specialTileInfos = boardData.MakingSpecialTiles(_pos);
		for(int i = 0; i < _specialTileInfos.Count; ++i)
		{
			_clearAndFillAct.AddSpecialTileInfo(_specialTileInfos[i]);
			_clearTargetPosList.AddRange(_specialTileInfos[i].clearedPosList);
		}

		//
		_clearTargetPosList.AddRange(boardData.ClearAllMatches(_pos));
		if(0 == _clearTargetPosList.Count) { return; }
		
		_clearAndFillAct.SetClearInfo(_clearTargetPosList);


		// 빈 타일들을 찾아 채우기
		for(int x = 0; x < xSize; x++)
		{
			for(int y = 0; y < ySize; y++)
			{
				if(boardData.IsEmpty(x, y))
				{
					_clearAndFillAct.AddFillInfo(x, y, boardData.Fill(x, y));
				}
			}
		}

		clearAndFillActList.Add(_clearAndFillAct);
	}

	private void Start()
	{
		InitEvent();
		InitBoardData();
		InitBoard();
		CreateBoard();
		InitAlarm();
    }

	void InitBoardData()
	{
		List<int> _tiilDatas = new List<int>();
		for(int i = 0; i < tiles.Count; ++i)
		{
			_tiilDatas.Add(i);
		}

		boardData = new BoardData(this, _tiilDatas);
	}

	void InitBoard()
	{
		board.Init(this, tiles, specialVerticalTiles, specialHorizontalTiles);
	}

	void CreateBoard()
	{
		boardData.CreateBoard(xSize, ySize);
		board.CreateBoard(tilePrefab, boardData.GetBoardTable(), xSize, ySize);

		move = MOVE_MAX;
		score = 0;
		panelGame.Set(move, score);
    }

	void InitAlarm()
	{
		StartHint();
    }

	void InitEvent()
	{
		EventBus.Subscribe<GameEventType>(OnGameEvent);
	}

	void StartHint()
	{
		hintAlarm.StartTimer(HINT_TIME, OnHintAlarm);
	}

	void StopHint()
	{
		hintAlarm.StopTimer();
		hintAlarm.StartTimer(HINT_TIME, OnHintAlarm);
		board.HideHint();
	}

	void OnHintAlarm()
	{
		board.ShowHint(boardData.FindHintTileIndexes());
	}

	void OnClick_Replay()
	{
		iTween.Stop();

		StopHint();
		CreateBoard();
		StartHint();
	}

	void OnGameEvent(GameEventType _e)
	{
		switch(_e)
		{
			case GameEventType.CLICK_REPLAY:
				OnClick_Replay();
                break;
        }
	}
}
